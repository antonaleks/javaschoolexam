package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers.isEmpty() || inputNumbers.size() > 50 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        TreeSet<Integer> set = new TreeSet<>(inputNumbers);

        int[][] array = getEmptyPyramid(set.size());
        Iterator<Integer> iter = set.iterator();
        for (int i = 0; i < array.length; i++) {
            for (int j = array.length - i - 1; j - i < array.length; j += 2) {
                array[i][j] = iter.next();
            }
        }
        return array;
    }

    private int[][] getEmptyPyramid(int lenght) {
        int rows = 0;
        for (int i = 1, sum = 0; sum < lenght; i++) {
            sum += i;
            rows = i;
        }
        int[][] array = new int[rows][rows * 2 - 1];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows * 2 - 1; j++) {
                array[i][j] = 0;
            }
        }
        return array;
    }

}
