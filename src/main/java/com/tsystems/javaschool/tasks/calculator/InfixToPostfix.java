package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class InfixToPostfix {

    private static String operators = "+-*/";
    private static String delimiters = "()" + operators;
    private static boolean flag = true;

    private static boolean isDelimiter(String token) {
        if (token.length() != 1) return false;
        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isOperator(String token) {
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    public static List<String> parse(String infix) {
        try {
            List<String> postfix = new ArrayList<>();
            Deque<String> stack = new ArrayDeque<>();
            StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
            String prev = "";
            String curr = "";
            while (tokenizer.hasMoreTokens()) {
                curr = tokenizer.nextToken();
                if (!prev.equals("") && isOperator(prev) && isOperator(curr)) return null;
                if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                    flag = false;
                    return null;
                } else if (isDelimiter(curr)) {
                    if (curr.equals("(")) stack.push(curr);
                    else if (curr.equals(")")) {
                        while (!stack.peek().equals("(")) {
                            postfix.add(stack.pop());
                            if (stack.isEmpty()) {
                                flag = false;
                                return null;
                            }
                        }
                        stack.pop();
                    } else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }
                        stack.push(curr);
                    }

                } else {
                    postfix.add(curr);
                }
                prev = curr;
            }

            while (!stack.isEmpty()) {
                if (isOperator(stack.peek())) postfix.add(stack.pop());
                else {
                    flag = false;
                    return null;
                }
            }
            return postfix;

        } catch (NullPointerException ex) {
            return null;
        }
    }
}
