package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class ParsePostfix {

    public static String calc(List<String> postfix) {
        if (postfix != null) {
            try {
                Deque<Double> stack = new ArrayDeque<>();
                for (String x : postfix) {
                    if (x.equals("+")) stack.push(stack.pop() + stack.pop());
                    else if (x.equals("-")) {
                        Double b = stack.pop(), a = stack.pop();
                        stack.push(a - b);
                    } else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
                    else if (x.equals("/")) {
                        Double b = stack.pop(), a = stack.pop();
                        if (b == 0) throw new IllegalArgumentException();
                        stack.push(a / b);
                    } else stack.push(Double.valueOf(x));
                }
                double result = stack.pop();
                return result == (long) result ? String.format("%d", (long) result) : String.format("%s", result);
            } catch (Exception ex) {
                return null;
            }
        } else return null;
    }
}